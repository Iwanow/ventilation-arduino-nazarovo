
//номера выходов
int outPins[] = {2,3,4,5,6,7,8,9,10,11,12,13}; 

//задержка после включения реле, мс
int delayOn = 500;  

//задержка после выключения реле, перед включением следующего, мс
int delayOff = 100;

//задержка между циклами, мс
int delayCycle = 1000 * 60 * 30;

//число циклов перед задержкой между циклами
int nCycle = 2;


int outsNumber = 0;
int outIndex = 0;
int cycleNumber = 0;

void setup() {
  outsNumber = sizeof(outPins) / sizeof(outPins[0]);

  //переключаем все пины в режим выхода и выключаем
  for(int i = 0; i < outsNumber; i++)
  {
    pinMode(outPins[i], OUTPUT);
    digitalWrite(outPins[i], LOW);
  }
}

void loop() {

  digitalWrite(outPins[outIndex], HIGH);
  delay(delayOn);
  digitalWrite(outPins[outIndex], LOW);
  delay(delayOff);

  
  outIndex++;
  if(outIndex >= outsNumber)
  {
    outIndex = 0;
    cycleNumber++;
    if(cycleNumber >= nCycle)
    {
      cycleNumber = 0;
	    delay(delayCycle);
    }
  }
}




